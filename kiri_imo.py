﻿# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#Kiri Imouto v1.0
#by Chtobi, 2014

from sys import exit
from os import path
import argparse


def createParser ():
    parser = argparse.ArgumentParser(prog = 'Kiri Imouto 1.0', description = '''Console utility for commenting in japanese lines,
    hyphenation of cyrillic translation(beta) and replacing normal spaces with nonbreaking ones in [setname name = ].
    Created for work with Kirikiri VN engine *.ks script files.
    All lines with ; in the beginning of the string will be ignored.
    Input file should be encoded in Unicode 2 Little-endian (UTF16 LE).
    Output file will be "inputfilename.ks" ''')

    parser.add_argument ('file_name', nargs = 1, type = argparse.FileType(mode='r', bufsize=-1), help = "Input text file name. Only UCS2 LE is allowed." )
    parser.add_argument ('-j', action='store_true', dest = 'comment', default = False, help = 'Comment in all japanese lines with ;.')
    parser.add_argument ('-y', action='store_true', dest = 'hyphenation', default = False, help = 'Make line folding with [r] in cyrillic lines.')
    parser.add_argument ('-s', action='store_true', dest = 'spaces', default = False, help = 'Replace all spaces in [setname name = ] with nonbreaking ones.')

    return parser



#---------------------------------------------
#Проверяем кириллицу
#Коды для диапазона кириллицы
#Cyrillic
#Range: 0400–04FF
def check_cyr(code):
    cyr1 = ord('\u0400')
    cyr2 = ord('\u04ff')

    if (code >= cyr1 and code <= cyr2):
        return True
    else:
        return False

#--------------------------------------------
#Japanese-style punctuation ( 3000 - 303f)
#Hiragana ( 3040 - 309f)
#Katakana ( 30a0 - 30ff)
#Full-width roman characters and half-width katakana ( ff00 - ffef)
#CJK unifed ideographs - Common and uncommon kanji ( 4e00 - 9faf)
#CJK unified ideographs Extension A - Rare kanji ( 3400 - 4dbf)
#http://www.rikai.com/library/kanjitables/kanji_codes.unicode.shtml
#Коды для диапазона японских символов:
def check_jps(code):
    #code - юникодный код проверяемого символа

    #Проверяем, является ли код символа японским
    jpu1 = ord('\u3000')
    jpu2 = ord('\u303f')

    hir1 = ord('\u3040')
    hir2 = ord('\u309f')

    kat1 = ord('\u30a0')
    kat2 = ord('\u30ff')

    rom1 = ord('\uff00')
    rom2 = ord('\uffef')

    kan1 = ord('\u4e00')
    kan2 = ord('\u9faf')

    rka1 = ord('\u3400')
    rka2 = ord('\u4dbf')

    if (code >= jpu1 and code <= jpu2):
        return True

    if (code >= hir1 and code <= hir2):
        return True

    if (code >= kat1 and code <= kat2):
        return True

    if (code >= rom1 and code <= rom2):
        return True

    if (code >= kan1 and code <= kan2):
        return True

    if (code >= rka1 and code <= rka2):
        return True

    return False



#---------------------------------------
#Выставляем перенос строки, если он нужен
def make_ln(line):

    #line_part = line #Проверка части всей строки

    #Программа проверяет строки только больше или равные symb_before_ln
    #Строки, где нет пробелов (ну а вдруг?) программа не рассматривает
    if ( len(line) >= symb_before_ln and (" " in line) ):
        #Программа не учитывает строки, в которых уже есть знак начала новой строки [r].
        #Но некоторые строки специально оканчиваются на [l][r].
        #В последнем случае, строка не учитывается, только если в ней число знаков [r] больше одного.
        if ( not ( "[r]" in line ) or ( line.endswith("[l][r]\n")  and ( line.count("[r]") == 1 ) ) ):
            line_n = line #Строка в процессе обработки
            line_all = "" #Окончательный вариант строки
            while ( len(line_n) >= symb_before_ln ):
                line_before_ln = line_n[:symb_before_ln] #Строка до того места, где нужен знак переноса.

                last_space = line_before_ln.rfind(" ") #Ищем последний пробел

                line_before_ln = line_n[:(last_space+1)] #Делаем еще один срез там, где найден пробел, с учетом разницы между индексом и номером

                line_before_ln += "[r]" #Добавляем символ перед пробелом

                line_after_ln = line_n[(last_space+1):] #Кусок текста после символа [r]

                #Соединяем куски

                if ( len(line_after_ln) < symb_before_ln):
                    line_all = line_all + line_before_ln + line_after_ln
                    return line_all
                else:
                    line_all = line_all + line_before_ln
                    line_n = line_after_ln


    #Если попалась неподходящая строка и условия не сработали - возвращаем исходную.
    return line


#--------------------------------------------------
#Обработка неразрывных пробелов
def nb_spaces(line):

    first_part = ( line.partition('[setname name=') )[1]
    #Выделяем из строки отдельно часть [setname name= из кортежа(часть [1])

    last_part = ( line.partition('[setname name=') )[2]
    #Выделяем часть с именем и закрывающей скобкой
    last_part = last_part.replace( chr(32), chr(160) )
    # 32 - код пробела, 160 - код неразрывного пробела.
    return first_part + last_part




#-----------------------------------------------------------




if __name__ == '__main__':
    #Парсер переметров
    parser = createParser()
    namespace = parser.parse_args()

    if ( namespace.comment == False and namespace.hyphenation == False and namespace.spaces == False ):
        print ("At least one parameter ( -j, -y or -s ) is required.")
        exit(0)


    #----------------------------
    #Кол-во символов, которое допускается в строке до символа перехода.
    symb_before_ln = 50
    #Вычисляется приблизительно.

    del_lines_dict = {} #Словарь символов, которые показывают, что строку нужно пропустить

    del_lines_dict = del_lines_dict.fromkeys( [ord('\u003b')] , "skip")
    #Пропускаем строки, начинающиеся с точки с запятой, их обрабатывать не надо
    #ord('\u003b') ;

    #Имя входного файла
    input_name = namespace.file_name[0].name

    #Имя выходного файла
    output_name = path.splitext(input_name)[0] + ".ks"

    #Открываем файл-скрипт
    with open(input_name, 'rt', encoding="utf_16_le") as input_file:

        with open(output_name, 'wt', encoding="utf_16_le") as output_file: #Открываем файл на запись

            for line in input_file:

                if ( del_lines_dict.get( ord(line[0]) ) == "skip" ): #Пропускаем строки с точкой с запятой без изменений,
                    #ord(line[0]) - код первого символа строки
                    output_file.write(line)
                    continue

                if ( line.startswith('[setname name=') == True and namespace.spaces == True): #Проверяем, не начинается ли строка с [setname name=
                    result_line = nb_spaces(line)

                    output_file.write(result_line)
                    continue


                #Проверять длину лениво, поэтому для безопасности дополняем строку до 4-х символов нулями(если она короче)
                line_check = line.ljust(4, "0")

                if ( namespace.hyphenation == True ):
                    #Проверяем, является ли один из символов(1,2,3,4) кириллическим
                    if ( check_cyr( ord(line_check[0]) ) or check_cyr( ord(line_check[1])) or check_cyr( ord(line_check[2])) or check_cyr( ord(line_check[3])) ):
                        result_line = make_ln(line)

                        output_file.write(result_line) #
                        continue

                if ( namespace.comment == True ):
                    #Проверяем, является ли один из символов(1,2,3,4) японским
                    if ( check_jps( ord(line_check[0]) ) or check_jps( ord(line_check[1]) ) or check_jps( ord(line_check[2]) ) or check_jps( ord(line_check[3]) ) ):

                        line = ';' + line #Добавляем к строке с японскими символами ; в начало

                        output_file.write(line) #
                        continue


                output_file.write(line)

