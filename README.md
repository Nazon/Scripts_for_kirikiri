# Kiri Imouto

Console utility for commenting in japanese lines, hyphenation of cyrillic translation(beta) and replacing normal spaces with non-breaking ones in [setname name = ].
Created for work with Kirikiri VN engine *.ks script files. All lines with ; in the beginning of the string will be ignored.
Input file should be encoded in Unicode 2 Little-endian (UTF16 LE).
Output file will be "inputfilename.ks".

## Usage:
``` kiri_imo.py inputfilename -j -y -s ```

### Keys:

* ``` inputfilename ```	Path to your input file.   
* ``` - j  ``` Comment in all lines, that starts with japanese symbols with ";" .  
* ``` - y ``` Put line breaks [r] in lines with cyrillic letters. Line length is 50 letters in the first version.  
* ``` - s ``` Change ordinary spaces with non-breaking ones in [setname name = ] constructions.  
* ``` - h ``` Help.  